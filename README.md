# pecan
![Screenshot](/screenshots/1.png)

A bar for macOS.  Reports — by default — the current workspace, network bandwidth, date, battery percentage and time.

## Instructions
`pecan` requires [Übersicht](http://tracesof.net/uebersicht/).  

Once Übersicht is installed, download this repository to wherever your widgets are stored (by default ~/Library/Application Support/Übersicht/widgets/).

This can be done via the terminal like so:
```
brew tap caskroom/cask
brew cask install ubersicht
brew install ical-buddy
git clone https://gitlab.com/marcel.pfeifer/bersicht-widgets.git "$HOME/Library/Application Support/Übersicht/widgets/pecan"
```

If Übersicht is running, then the bar should appear.

## Customization & Themes

#### Load themes

`pecan` includes a tool `pecan-style` to allow users to save and load themes.  There are, by default, a few included themes.

```
ln -s "${HOME}/Library/Application Support/Übersicht/widgets/pecan/pecan-style" "/usr/local/bin/pecan-style"
```

Then if you want to load the default theme,  you do it like so:

```
pecan-style --load default
```

For more info on `pecan-style` do:
```
pecan-style --help
```

#### Create themes

Because `pecan` is styled using CSS3 variables, the top lines of `style.css` can easily be edited to change properties like opacity, alignment, padding, colors and more.  If you are using this bar with Wal, then you should be editing `scss/style.scss` instead.
