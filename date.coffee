command: "date +\"%A, %d %B %Y\""

refreshFrequency: 100000

render: (output) ->
  "<div class='screen'><div class='holder'><div class='pecandate'>#{output}</div></div></div>"
